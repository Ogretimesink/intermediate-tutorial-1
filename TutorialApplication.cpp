/*
-----------------------------------------------------------------------------
Filename:	TutorialApplication.cpp
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//-------------------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: mShutdown(false)
	, mRoot(0)
	, mCamera(0)
	, mSceneMgr(0)
	, mWindow(0)
	, mResourcesCfg(Ogre::StringUtil::BLANK)
	, mPluginsCfg(Ogre::StringUtil::BLANK)
	, mCameraMan(0)
	, mMouse(0)
	, mKeyboard(0)
	, mInputMgr(0)
	, mDistance(0)
	, mWalkSpd(70.0)
	, mDirection(Ogre::Vector3::ZERO)
	, mDestination(Ogre::Vector3::ZERO)
	, mAnimationState(0)
	, mEntity(0)
	, mNode(0)
{
}
//-------------------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
	// Destroy the basic camera controller object
	if (mCameraMan) delete mCameraMan;
 
	// Remove the application as a Window listener
	Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);

	// Call function to unattach OIS before window shutdown
	windowClosed(mWindow);
 
	// Delete the root object instance for the application
	delete mRoot;
}
//-------------------------------------------------------------------------------------
void TutorialApplication::go()
{
	#ifdef _DEBUG
		// Set the debug build resource configuration files for static libraries
		mResourcesCfg = "resources_d.cfg";

		// Set the debug build plugin configuration files for static libraries
		mPluginsCfg = "plugins_d.cfg";
	#else
		// Set the debug build resource configuration files for non-static libraries
		mResourcesCfg = "resources.cfg";

		// Set the debug build plugin configuration files for non-static libraries
		mPluginsCfg = "plugins.cfg";
	#endif
 
	// Call function to set up individual parts of the application
	if (!setup())
		return;
 
	// Start the automatic scene rendering cycle
	mRoot->startRendering();
 
	// Call function to clean up objects before shutdown
	destroyScene();
}
//---------------------------------------------------------------------------
bool TutorialApplication::setup()
{
	// Create root object instance for the application
	mRoot = new Ogre::Root(mPluginsCfg);
 
	// Call function to load resources from configuration files
	setupResources();
 
	// Call function to show the configuration dialog box
	if (!configure())
		return false;
 
	// Call function to initialize the scene manager and OverlaySystem objects 
	chooseSceneManager();

	// Call function to initialize the scene camera
	createCamera();

	// Call function to initialize the scene viewports
	createViewports();
 
	// Set default mipmap level (NB some APIs ignore this)
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
 
	// Call function to create any resource listeners for loading screens
	createResourceListener();

	// Call function to initialize all of the resources found by the application
	loadResources();
 
	// Call function to fill the scene with graphic objects
	createScene();
 
	// Call function to initialize input objects
	createFrameListener();
 
	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::setupResources()
{
	// Declare object for configuration file
	Ogre::ConfigFile cf;

	// Load settings data from configuration file
	cf.load(mResourcesCfg);
 
	// Declare strings into which to load configuration data
	Ogre::String secName, typeName, archName;

	// Declare an iterator to load sections of the resource configuration file
	Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();
 
	// Iterate through the resource configuration file
	while (secIt.hasMoreElements())
	{
		// Get the current element without advancing
		secName = secIt.peekNextKey();

		// Declare multimap settings pair and load a section
		Ogre::ConfigFile::SettingsMultiMap* settings = secIt.getNext();

		// Declare iterator for multimap items in section
		Ogre::ConfigFile::SettingsMultiMap::iterator setIt;
 
		// Loop through multimap settings in the section
		for (setIt = settings->begin(); setIt != settings->end(); ++setIt)
		{
			// Unpack the resource location type
			typeName = setIt->first;

			// Unpack the resource location path
			archName = setIt->second;

			// Add the unpacked resources to the application
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
		}
	}
}
//---------------------------------------------------------------------------
bool TutorialApplication::configure()
{
	// Load ogre.cfg file, otherwise show the configuration dialog box
	if (!(mRoot->restoreConfig() || mRoot->showConfigDialog()))
	{
		return false;
	}
 
	// Initialize the rendering window using the chosen rendering system
	mWindow = mRoot->initialise(true, "ITutorial");
 
	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::chooseSceneManager()
{
	// Initialize the scene manager object
	mSceneMgr = mRoot->createSceneManager(Ogre::ST_EXTERIOR_CLOSE);
}
//---------------------------------------------------------------------------
void TutorialApplication::createCamera()
{
	// Initialize the scene camera
	mCamera = mSceneMgr->createCamera("PlayerCam");
 
	// Position the camera within the scene
	mCamera->setPosition(Ogre::Vector3(0, 0, 80));

	// Set a position to point the camera
	mCamera->lookAt(Ogre::Vector3(0, 0, -300));

	// Set the distance the camera won't render meshes
	mCamera->setNearClipDistance(5);
 
	// Initialize the default camera controller
	mCameraMan = new OgreBites::SdkCameraMan(mCamera);
}
//---------------------------------------------------------------------------
void TutorialApplication::createViewports()
{
	// Declare and initialize a scene rendering viewpoint for the camera
	Ogre::Viewport* vp = mWindow->addViewport(mCamera);

	// Set the scene background color
	vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
 
	// Set the viewport size
	mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//---------------------------------------------------------------------------
void TutorialApplication::createResourceListener()
{
}
//---------------------------------------------------------------------------
void TutorialApplication::loadResources()
{
	// Initialize all of the resources found by the application
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene()
{
	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
 
	// Create an instance of a computer graphic model robot
	mEntity = mSceneMgr->createEntity("robot.mesh");

	// Create an object in the scene and set its position
	mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(0, 0, 25.0));

	// Add the robot entity to the object
	mNode->attachObject(mEntity);

	// Add the first destination to the robot destination list
	mWalkList.push_back(Ogre::Vector3(550.0, 0, 50.0));

	// Add the second destination to the robot destination list
	mWalkList.push_back(Ogre::Vector3(-150.0, 0, -200.0));

	// Add the third destination to the robot destination list
	mWalkList.push_back(Ogre::Vector3(0, 0, 25.0));

	// Add the final destination to the robot destination list
	mWalkList.push_back(Ogre::Vector3(550.0, 0, 50.0));
 
	// Computer graphic model instance
	Ogre::Entity* ent;

	// Scene object
	Ogre::SceneNode* node;
 
	// Create first instance of a computer graphic model knot
	ent = mSceneMgr->createEntity("knot.mesh");

	// Create an object in the scene and set its position
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(0, -10.0, 25.0));

	// Add the knot entity to the object
	node->attachObject(ent);

	// Reset the size of the object
	node->setScale(0.1f, 0.1f, 0.1f);
 
	// Create second instance of a computer graphic model knot
	ent = mSceneMgr->createEntity("knot.mesh");

	// Create an object in the scene and set its position
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(550.0, -10.0, 50.0));

	// Add the knot entity to the object
	node->attachObject(ent);

	// Reset the size of the object
	node->setScale(0.1f, 0.1f, 0.1f);
 
	// Create final instance of a computer graphic model knot
	ent = mSceneMgr->createEntity("knot.mesh");

	// Create an object in the scene and set its position
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(-150.0, -10.0, -200.0));

	// Add the knot entity to the object
	node->attachObject(ent);

	// Reset the size of the object
	node->setScale(0.1f, 0.1f, 0.1f);

	// Set a position to point the camera
	mCamera->setPosition(90.0, 280.0, 535.0);

	// Move the camera angle down/up
	mCamera->pitch(Ogre::Degree(-30.0));

	// Rotate the camera angle left/right
	mCamera->yaw(Ogre::Degree(-15.0));

	// Retrieve the "Idle" animation state of the robot
	mAnimationState = mEntity->getAnimationState("Idle");

	// Set the animation to loop upon finishing
	mAnimationState->setLoop(true);

	// Set the animation to run
	mAnimationState->setEnabled(true);
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener()
{
	// Write message to log about the start of the initialization of input devices
	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
 
	// Create a list of parameters
	OIS::ParamList pl;

	// Declare unsigned integer for the window handle
	size_t windowHnd = 0;

	// Declare stream to name for the window handle
	std::ostringstream windowHndStr;
 
	// Get the window handle
	mWindow->getCustomAttribute("WINDOW", &windowHnd);

	// Bitwise left shift?? 
	windowHndStr << windowHnd;

	// Pass the name and window handle as a pair to the list
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
 
	// Create the input device manager
	mInputMgr = OIS::InputManager::createInputSystem(pl);
 
	// Create the keyboard input device object
	mKeyboard = static_cast<OIS::Keyboard*>(mInputMgr->createInputObject(OIS::OISKeyboard, true));

	// Create the mouse input device object
	mMouse = static_cast<OIS::Mouse*>(mInputMgr->createInputObject(OIS::OISMouse, true));
 
	// Register as a keyboard listener
	mKeyboard->setEventCallback(this);

	// Register as a mouse listener
	mMouse->setEventCallback(this);
 
	// Set initial mouse clipping size
	windowResized(mWindow);
 
	// Register the window as a Window listener
	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
 
	// Add this function createFrameListener as a frame listener
	mRoot->addFrameListener(this);
 
	// Write message to log about the end of the initialization of input devices
	Ogre::LogManager::getSingletonPtr()->logMessage("Finished");
}
//---------------------------------------------------------------------------
void TutorialApplication::windowResized(Ogre::RenderWindow* rw)
{
	// Declare rendering area variables
	unsigned int width, height, depth;

	// Declare rendering area variables
	int left, top;

	// Get information about the rendering area
	rw->getMetrics(width, height, depth, left, top);
 
	// Retrieve the current state of the mouse
	const OIS::MouseState& ms = mMouse->getMouseState();

	// Set the mouse clipping area width to that of the display area
	ms.width = width;

	// Set the mouse clipping area height to that of the display area
	ms.height = height;
}
//---------------------------------------------------------------------------
void TutorialApplication::windowClosed(Ogre::RenderWindow* rw)
{
	// Check for valid application window
	if (rw == mWindow)
	{
		// Check for valid input device manager
		if (mInputMgr)
		{
			// Destroy the mouse input device object
			mInputMgr->destroyInputObject(mMouse);

			// Destroy the keyboard input device object
			mInputMgr->destroyInputObject(mKeyboard);
 
			// Destroy the input device manager
			OIS::InputManager::destroyInputSystem(mInputMgr);

			// Re-initialize the input device manager before application shutdown
			mInputMgr = 0;
		}
	}
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Check for the escape key
	if (mKeyboard->isKeyDown(OIS::KC_ESCAPE))

		// Set the flag to shutdown the applicaion
		mShutdown = true;
 
	// Check the application shutdown flag
	if (mShutdown)

		// End the application
		return false;
 
	// Check the windows status
	if (mWindow->isClosed())

		// End the application
		return false;

	// Check current robot direction
	if (mDirection == Ogre::Vector3::ZERO)
	{
		// Check for more robot destinations
		if (nextLocation())
		{
			// Retrieve the "Walk" animation state of the robot
			mAnimationState = mEntity->getAnimationState("Walk");

			// Set the animation to loop upon finishing
			mAnimationState->setLoop(true);

			// Set the animation to run
			mAnimationState->setEnabled(true);
		}
	}
	else
	{
		// Calculate the distance the robot moved this frame
		Ogre::Real move = mWalkSpd * fe.timeSinceLastFrame;

		// Calculate the distance the robot still needs to move to destination
		mDistance -= move;
 
		// Check the robots distance from destination
		if (mDistance <= 0.0)
		{
			// Set the robots location to the destination
			mNode->setPosition(mDestination);

			// Initialize the robot movement direction
			mDirection = Ogre::Vector3::ZERO;
 
			// Check for more destinations
			if (nextLocation())
			{
				// Find the direction the robot is currently facing
				Ogre::Vector3 src = mNode->getOrientation() * Ogre::Vector3::UNIT_X;
 
				// Check the rotation needed to turn to face the destination
				if ((1.0 + src.dotProduct(mDirection)) < 0.0001)
				{
					// Turn the robot
					mNode->yaw(Ogre::Degree(180));
				}
				else
				{
					// Find the rotation needed to turn to face the destination
					Ogre::Quaternion quat = src.getRotationTo(mDirection);

					// Turn the robot
					mNode->rotate(quat);
				}
			}
			else
			{
				// Retrieve the "Idle" animation state of the robot
				mAnimationState = mEntity->getAnimationState("Idle");

				// Set the animation to loop upon finishing
				mAnimationState->setLoop(true);

				// Set the animation to run
				mAnimationState->setEnabled(true);
			}
		}
		else
		{
			// Move the robot towards the destination
			mNode->translate(move * mDirection);
		}
	}
 
	// Check the keyboard input device
	mKeyboard->capture();

	// Check the mouse input device
	mMouse->capture();
 
	// Progress the robot animation
	mAnimationState->addTime(fe.timeSinceLastFrame);
 

	mCameraMan->frameRenderingQueued(fe);
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::nextLocation()
{
	// Check for robot destinations
	if (mWalkList.empty())

		// No more destinations
		return false;
 
	// Retrieve the next robot destination
	mDestination = mWalkList.front();

	// Retrieve the next robot destination
	mWalkList.pop_front();

	// Set the robots direction
	mDirection = mDestination - mNode->getPosition();

	// Normalise the direction vector to find the distance to the destination
	mDistance = mDirection.normalise();
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Send keyboard input to the OgreBites camera controller
	mCameraMan->injectKeyDown(ke);
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Send keyboard input to the OgreBites camera controller
	mCameraMan->injectKeyUp(ke);
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Send mouse input to the OgreBites tray manager
	mCameraMan->injectMouseMove(me);
 
	return true;
}

//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Send mouse input to the OgreBites tray manager
	mCameraMan->injectMouseDown(me, id);
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Send mouse input to the OgreBites tray manager
	mCameraMan->injectMouseUp(me, id);
 
	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene()
{
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	// For windows playtform
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	// For linux and apple platform
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			// Display windows error message box
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			// Display error message to linux and apple standard output error stream
			std::cerr << "An exception has occurred: " << e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
